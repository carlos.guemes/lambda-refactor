#!/bin/bash
./remove.sh

pip3 install  --target ./lambda_function_package requests
pip3 install redis --target ./lambda_function_package requests
pip3 install geoip2 --target ./lambda_function_package requests
pip3 install idna_ssl --target ./lambda_function_package requests

cd lambda_function_package/ && zip -r ../lambda_function_package.zip . && cd .. && zip -g lambda_function_package.zip lambda_function.py