from urllib.parse import parse_qs

import boto3
import json
from preprocessing import *
import time
import datetime

# Constant to be used in case of unkown values
UNKNOWN = '-'
SEPARATOR = '_$$_'

def lambda_handler(event, context):
    # get kinesis client
    client = boto3.client('kinesis')
    # parse incoming form data
    print('## EVENT')
    parsedEvent = parse_qs(event)
    print(parsedEvent)
    # logging.debug(event)
    # print(event)
    # Seleccionar tipo de evento a través del campo http.initiator
    if 'http.initiator' in parsedEvent:
        if parsedEvent['http.initiator'][0] == "xhr":
            preprocessing = Preprocessing_xhr()
        elif parsedEvent['http.initiator'][0] in ["spa_hard", "spa"]:
            preprocessing = Preprocessing_spa()
        elif parsedEvent['http.initiator'][0] == "error":
            preprocessing = Preprocessing_err()
        preprocessing.clean_and_normalize(parsedEvent)
        preprocessing.extract_fields()
        print("sending message")
        preprocessing.result["key_timestamp"] = int(datetime.datetime.utcnow().timestamp() * 1000)
        response = client.put_record(
            StreamName='test-kinesis',
            Data=preprocessing.prefix()+'@'+json.dumps(preprocessing.result),
            PartitionKey='0',
        )
        print("Message sent:", response)
        print(str(time.perf_counter()))
    return {
        'statusCode': 200
    }
