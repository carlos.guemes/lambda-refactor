SEPARATOR = '_$$_'
UNKNOWN = '-'

from user_agents import parse
from geolite2 import geolite2
import redis
redis_connection = redis.Redis(host='cluster-redis.zmddku.0001.euc1.cache.amazonaws.com', port=6379,
                                            decode_responses=True)
def find_country(ip):
    if ip == UNKNOWN:
        return UNKNOWN

    #print("Query redis")
    redis_key = "Country_IP_" + ip
    country = redis_connection.get(redis_key)
    if country is not None:
        return country
    #print("Query redis done")

    # Not a hit, use geolite
    #print("Query geolite")
    geo_data = geolite2.reader().get(ip)
    geolite2.close()
    #print("Query geolite done")
    country = geo_data['country']['iso_code']
    redis_connection.set(redis_key, country)
    return country

class Preprocessing_raw(object):
    
    def prefix(self):
        raise NotImplementedError('Prefix has not been defined')

    def __init__(self):
        self.result = dict()

    def clean_and_normalize(self,line):
        # Identifier key for the time entry, late used for aggregation
        # Timestamp truncated by minute
        self.result['key_timestamp'] = int(line['created'][0])
        fluent_tag_split = line['dv.fluenttag'][0].split('.')
        # Client name
        self.result['key_client'] = fluent_tag_split[-1] #tag.client
        # Source name
        self.result['key_type'] = fluent_tag_split[1] #tag.source
        # Source type
        self.result['key_subtype'] = fluent_tag_split[2] #tag.source_type
        # Source id
        self.result['key_source_id'] = fluent_tag_split[3] #tag.source_id
        # Parent source name
        # self.result['key_parent'] = line['dv.fluenttag'][0] #tag.parent
        # Parent source type
        # self.result['key_parent_type'] = line['dv.fluenttag'][0] #tag.parent_type
        # Parent source id
        # self.result['key_parent_id'] = line['dv.fluenttag'][0] #tag.parent_id
        # Normalized URL
        self.result['norm_url'] = line['source'][0].split('?')[0]

        # Potential error
        if 'err' in line:
            self.result['err-count'] = 1
            user_agent_fields = self.user_agent_information(line)
            self.result['ua_device'] = user_agent_fields['ua_device']
            self.result['browser_family'] = user_agent_fields['browser_family']
            self.result['browser_version'] = user_agent_fields['browser_version']

        
    def extract_fields(self):
        pass

    @staticmethod
    def list_get(dictionary, key, default):
        if key in dictionary:
            return dictionary[key][0]
        else:
            return default

    @staticmethod
    def user_agent_information(line):
        result = {}
        try:
            _user_agent = parse(line["userAgent"][0])
            ua_version = _user_agent.browser.version_string.split(".")
            result['browser_family'] = _user_agent.browser.family
            result['browser_version'] = _user_agent.browser.family + '|' + ua_version[0]
            #result['ua_os'] = _user_agent.os.family + '|' + _user_agent.os.version_string
            result['ua_os_family'] = _user_agent.os.family
            result['ua_device'] = _user_agent.device.family + '|' + str(_user_agent.device.model) + '|' +\
                                  str(_user_agent.device.brand)
        except Exception as e:
            print("Couldn't detect browser info due to Exception {}".format(e))
            result['browser_family'] = UNKNOWN
            result['browser_version'] = UNKNOWN
            #result['ua_os'] = UNKNOWN
            result['ua_os_family'] = UNKNOWN
            result['ua_device'] = UNKNOWN
        return result

class Preprocessing_xhr(Preprocessing_raw):

    def prefix(self):
        return 'xhr'

    def clean_and_normalize(self,line):
        super().clean_and_normalize(line)
        self.result['xhr-time'] = int(self.list_get(line, 't_done', 0))
        self.result['url'] = line['source'][0]
        # In XHR, in case the method isn't specified we assume it's a GET
        self.result['xhr-method'] = self.list_get(line, 'http.method', 'GET')
        self.result['xhr-errno'] = self.list_get(line, 'http.errno', UNKNOWN)
    def extract_fields(self):
        super().extract_fields()
        self.result['xhr-count-initiator'] = 1
        self.result['xhr-count'] = 1

class Preprocessing_spa(Preprocessing_raw):

    
    def prefix(self):
        return 'spa'
    
    user_agent_fields = None
    
    def clean_and_normalize(self,line):
        
        super().clean_and_normalize(line)
        self.result['pag-time-load'] = int(self.list_get(line, 't_done', 0))
        self.result['url'] = line['source'][0]
        self.result['mob-ct'] = self.list_get(line, 'mob.ct', UNKNOWN)
        self.result['mob-bw'] = self.list_get(line, 'mob.bw', UNKNOWN)
        #print("Finding country...")
        self.result['country'] = find_country(self.list_get(line, 'clientIp', UNKNOWN))
        #print("Country found...")
        self.result['session_id'] = self.list_get(line, 'rt.si', UNKNOWN)
        
        # perf_redirect Obtained from substracting nt_nav_st of nt_red_end or nt_red_st of nt_red_end
        if 'nt_red_end' in line and 'nt_nav_st' in line:
            self.result['perf_redirect'] = int(line['nt_red_end'][0]) - int(line['nt_nav_st'][0])
        elif 'nt_red_end' in line and 'nt_red_st' in line:
            self.result['perf_redirect'] = int(line['nt_red_end'][0]) - int(line['nt_red_st'][0])
        
        # perf_appcache perf_dns
        if 'nt_dns_st' in line:
            # Obtained from substracting nt_fet_st of nt_dns_st or nt_red_end of nt_dns_st
            if 'nt_fet_st' in line:
                self.result['perf_appcache'] = int(line['nt_dns_st'][0]) - int(line['nt_fet_st'][0])
            elif 'nt_red_end' in line:
                self.result['perf_appcache'] = int(line['nt_dns_st'][0]) - int(line['nt_red_end'][0])
            # Obtained from substracting nt_dns_st of nt_dns_end
            if 'nt_dns_end' in line:
                self.result['perf_dns'] = int(line['nt_dns_end'][0]) - int(line['nt_dns_st'][0])

        # perf_tcp Obtained from substracting nt_con_st of nt_con_end
        if 'nt_con_end' in line and 'nt_con_st' in line:
            self.result['perf_tcp'] = int(line['nt_con_end'][0]) - int(line['nt_con_st'][0])

        # perf_request Obtained from substracting nt_req_st of nt_res_st
        if 'nt_res_st' in line and 'nt_req_st' in line:
            self.result['perf_request'] = int(line['nt_res_st'][0]) - int(line['nt_req_st'][0])

        # perf_response dom_time Obtained from substracting nt_res_st of nt_res_end
        if 'nt_res_end' in line:
            if 'nt_res_st' in line:
                self.result['perf_response'] = int(line['nt_res_end'][0]) - int(line['nt_res_st'][0])
            # Dom interactive: browser has ended parsing the HTML and DOM creation is completed.
            # Obtained from substracting nt_res_end from nt_domint
            if 'nt_domint' in line:
                self.result['pag-time-load' + SEPARATOR + 'dom'] = int(line['nt_domint'][0]) - int(line['nt_res_end'][0])

        # perf_unload Obtained from substracting nt_unload_st from nt_unload_end
        if 'nt_unload_end' in line and 'nt_unload_st' in line:
            self.result['perf_unload'] = int(line['nt_unload_end'][0]) - int(line['nt_unload_st'][0])

        # perf_processing Obtained from substracting nt_domloading from nt_domcomp
        if 'nt_domcomp' in line and 'nt_domloading' in line:
            self.result['perf_processing'] = int(line['nt_domcomp'][0]) - int(line['nt_domloading'][0])

        # perf_domcontentloaded Obtained from substracting nt_domcontloaded_st from nt_domcontloaded_end
        if 'nt_domcontloaded_end' in line and 'nt_domcontloaded_st' in line:
            self.result['perf_domcontentloaded'] = int(line['nt_domcontloaded_end'][0]) - int(line['nt_domcontloaded_st'][0])

        # perf_load render_time Obtained from substracting nt_load_st from nt_load_end
        if 'nt_load_end' in line:
            if 'nt_load_st' in line:
                self.result['perf_load'] = int(line['nt_load_end'][0]) - int(line['nt_load_st'][0])
            # Render: The page and all the secondary resources are downloaded and ready. The loading bar has stopped.
            # Obtained from substracting nt_doming from nt_load_end
            if 'nt_domint' in line:
                self.result['pag-time-load' + SEPARATOR + 'render'] = int(line['nt_load_end'][0]) - int(line['nt_domint'][0])

        # network_time
        if 't_resp' in line:
            self.result['pag-time-load' + SEPARATOR + 'network'] = int(line['t_resp'][0])

        user_agent_fields = self.user_agent_information(line)
        for key, value in user_agent_fields.items():
            self.result[key] = value

        # spa-count-initiator or spa_hard-count-initiator
        self.result[line['http.initiator'][0] + '-count-initiator'] = 1
        
    def extract_fields(self):
        super().extract_fields()


        self.result['pag-count'] = 1


        self.result['pag-mobile-count'] = int(self.result['mob-ct'] != 0)


class Preprocessing_err(Preprocessing_raw):

    
    def prefix(self):
        return 'err'

    def clean_and_normalize(self, line):
        super().clean_and_normalize(line)
        user_agent_fields = self.user_agent_information(line)
        self.result['ua_device'] = user_agent_fields['ua_device']
        self.result['browser_family'] = user_agent_fields['browser_family']
        self.result['browser_version'] = user_agent_fields['browser_version']


    def extract_fields(self):
        super().extract_fields()
        self.result['err-count-initiator'] = 1
        self.result['err-count'] = 1

