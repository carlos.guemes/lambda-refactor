import datetime

import redis
import json
import decimal
import mysql.connector
from mysql.connector import Error
import base64
from dateutil.parser import isoparse
import requests

endpoint_url = "http://app.datavision.space/alerts"

class DecimalEncoder(json.JSONEncoder):
    """
    JSONEncoder expanded dates; modified way to handle floating point numbers.
    """

    def default(self, o):
        # Transform decimal points into floating point representation
        if isinstance(o, decimal.Decimal):
            return float(o)
        # Transform dates into iso format
        if isinstance(o, (datetime.date, datetime.datetime)):
            return o.isoformat()
        # Other variables continue with default behaviour
        return super().default(o)

def parseMySQLParameters(query, list):

    format_strings = ', '.join(['"{}"'] * len(list))
    query = query.format(format_strings)
    query = query.format(*list)

    return query


def getDataSourceID(r, sql_connection, client_key_name, ds_source_id, ds_type, ds_subtype):
    """
    Given a redis connection, a SQL connection and a set of identifier fields, return the id of the given datasource.
    :param r: Redis connection
    :param sql_connection: SQL connection
    :param client_key_name: name of the client that the source belongs to
    :param ds_source_id: sourceId of the datasource (not to be confused with the id of the datasource within the
    SQL)
    :param ds_type: type of the datasource
    :param ds_subtype: subtype of the datasource
    :return: id of the data source within the SQL
    """
    redis_key = client_key_name + ds_source_id + ds_type + ds_subtype
    redis_key_source_id = redis_key + "source_id"
    redis_key_client_id = redis_key + "cliend_id"
    source_id = r.get(redis_key_source_id)
    client_id = r.get(redis_key_client_id)

    # If hit, return result
    if source_id is not None:
        return source_id, client_id

    # Else: not in redis; search in the SQL
    sql_query = 'SELECT data_source.id, data_source.client_id  FROM data_source JOIN clients ON ' \
                'data_source.client_id = clients.id WHERE data_source.type = "{}" AND data_source.subtype = "{}" AND ' \
                'data_source.source_id = "{}" AND clients.key_name = "{}";'
    cursor = sql_connection.cursor()
    cursor.execute(sql_query.format(ds_type, ds_subtype, ds_source_id, client_key_name))
    source_id = cursor.fetchone()
    cursor.fetchall()

    # If hit, update redis and return result
    if source_id is not None:
        r.set(redis_key_source_id, source_id[0])
        r.set(redis_key_client_id, source_id[1])
        return tuple(source_id)

    # Else miss in MySQL need to insert also widgets with dashboards
    # First obtain the client
    sql_query = 'SELECT id FROM clients WHERE key_name = "{}" LIMIT 1;'
    cursor = sql_connection.cursor()
    cursor.execute(sql_query.format(client_key_name))
    client_id = cursor.fetchone()[0]
    cursor.fetchall()

    sql_query = 'INSERT INTO `dvisiondb`.`data_source`(`client_id`,`parent_id`,`key_name`,`name`,`type`,`subtype`,' \
                '`source_id`,`path`) VALUES("{}",0,"{}","{}","{}","{}","{}","");'
    key_name = ds_type + "/" + ds_subtype + "/" + ds_source_id
    cursor = sql_connection.cursor()
    cursor.execute(sql_query.format(client_id, key_name, key_name, ds_type, ds_subtype, ds_source_id))
    sql_connection.commit()
    source_id = cursor.lastrowid
    cursor.fetchall()
    # If the value is still missing, return 0
    if source_id == 0 or source_id is None:
        return 0, 0
    # Else, if it's a hit return
    else:
        r.set(redis_key_source_id, source_id)
        r.set(redis_key_client_id, client_id)
        return source_id, client_id


def create_datapoints(sql_connection, myMap, clean_input, ds_source_id):

    sqlInsert = "INSERT INTO data_agg_" + \
                clean_input['windowSize'] + \
        "(`data_metric_id`,`start_time`,`value_avg`,`value_acc`,`value_max`,`value_min`)VALUES(%s,%s,%s,%s,%s,%s)"

    cursor = sql_connection.cursor()
    for metric in clean_input['metrics']:
        cursor.execute(sqlInsert, (myMap[str(ds_source_id) + metric['type'] + metric['subtype']],
                                   clean_input["windowTime"], metric['avg'], metric['acc'], metric['max'],
                                   metric['min']))
        sql_connection.commit()


def get_metric_ids_and_create_datapoints(r, clean_input, ds_source_id, sql_connection, client_id):

    # For each metric, find its metric id from redis
    str_ds_source_id = str(ds_source_id)
    keys = [str_ds_source_id + metric['type'] + metric['subtype'] for metric in clean_input['metrics']]
    values = r.mget(keys=keys)
    myMap = dict(zip(keys, values))
    # Obtain a dict with those metrics with unknown id
    notFoundMetrics = [key for key, value in myMap.items() if value is None]

    if len(notFoundMetrics) != 0:
        # Find metric ids from the sql
        query = "SELECT  id,data_source_id,type,subtype FROM data_metric WHERE CONCAT(data_metric.data_source_id," \
                "data_metric.type,data_metric.subtype) IN ({})"
        query = parseMySQLParameters(query, notFoundMetrics)
        cursor = sql_connection.cursor()
        cursor.execute(query)
        results = cursor.fetchall()
        foundIDs = {str(data_source_id) + type + subtype: id for (id, data_source_id, type, subtype) in results}

        # to insert in MySQL
        metricsToCreate = [[ds_source_id, key['name'], key['subname'], key['type'], key['subtype']]
                           for key in clean_input['metrics']
                           if str_ds_source_id + key['type'] + key['subtype'] not in foundIDs
                           and str_ds_source_id + key['type'] + key['subtype'] in notFoundMetrics]
        if len(metricsToCreate) != 0:  # metrics to insert in MySQL

            query = "INSERT INTO `data_metric`(`data_source_id`,`name`,`subname`,`type`,`subtype`)VALUES(%s,%s,%s,%s,%s)"
            for metricToCreate in metricsToCreate:
                cursor.execute(query, tuple(metricToCreate))
                sql_connection.commit()
                insertedID = cursor.lastrowid
                foundIDs[str(metricToCreate[0]) + metricToCreate[3] + metricToCreate[4]] = insertedID

        r.mset(foundIDs)
        for key, value in foundIDs.items():
            myMap[key] = value

    # Create the datapoints with the selected ids
    sqlInsert = "INSERT INTO data_agg_" + clean_input['windowSize'] + \
                "(`data_metric_id`,`start_time`,`value_avg`,`value_acc`,`value_max`,`value_min`)VALUES(%s,%s,%s,%s,%s,%s)"
    # Add delete to remove redudant data beforehand
    sqlDelete = "DELETE FROM data_agg_" + clean_input['windowSize'] +\
                " WHERE `data_metric_id` = %s AND `start_time` = %s;"

    cursor = sql_connection.cursor()
    for metric in clean_input['metrics']:
        metric_id = myMap[str_ds_source_id + metric['type'] + metric['subtype']]
        cursor.execute(sqlDelete, (metric_id, clean_input["windowTime"]))
        sql_connection.commit()
        cursor.execute(sqlInsert, (metric_id, clean_input["windowTime"], metric['avg'], metric['acc'], metric['max'],
                                   metric['min']))
        sql_connection.commit()
        print("Metric inserted:", str_ds_source_id, metric['type'], metric['subtype'], clean_input["windowTime"])
        # Perform anomaly detection
        if metric["anomalyDetection"]:
            anomaly_detection(r, sql_connection, metric, metric_id, clean_input['windowSize'], clean_input["windowTime"],
                              ds_source_id, client_id)


def anomaly_detection(r, sql_connection, metric, metric_id, window_size, window_time, ds_source_id, client_id):
    # Find predicted metric (if applicable)
    data_source_id = ds_source_id
    metric_type = metric['type'] + '_forecast'
    metric_subtype = metric['subtype']
    redis_key = data_source_id + metric_type + metric_subtype
    prediction_metric_id = r.get(redis_key)

    # If miss, query database,
    if prediction_metric_id is None:
        sqlQuery = 'SELECT id FROM data_metric WHERE `data_source_id` = %s AND `type` = %s AND `subtype` = %s'
        cursor = sql_connection.cursor()
        cursor.execute(sqlQuery, (data_source_id, metric_type, metric_subtype))
        prediction_metric_id = cursor.fetchone()
        # If the value is still missing, return -1
        if prediction_metric_id is None:
            prediction_metric_id = -1
        else:
            prediction_metric_id = prediction_metric_id[0]
        # Update redis
        r.set(redis_key, prediction_metric_id)

    # print("prediction_metric_id", prediction_metric_id)

    # Only continue if a metric is obtained
    if prediction_metric_id is not None and prediction_metric_id > 0:
        # Read predicted minimum and maximums
        sqlQuery = 'SELECT value_max, value_min FROM data_agg_{} WHERE `data_metric_id` = "{}" AND `start_time` = "{}"'
        cursor = sql_connection.cursor()
        cursor.execute(sqlQuery.format(window_size, prediction_metric_id, window_time))
        value = cursor.fetchone()
        # Assuming we have a prediction
        if value is None:
            print("Error: a prediction was expected, but none was given!")
        else:
            min, max = tuple(value)
            # Check if the value is an anomaly
            if metric['acc'] > max:
                status = 'high'
            elif metric['acc'] < min:
                status = 'low'
            else:
                status = 'normal'

            # If status isn't normal, we need to record the anomaly
            if status != "normal":
                # Send anomaly through endpoint
                alert_data_send = {
                    "type": "anomaly",
                    "body": {
                        'clientID': client_id,
                        'dataMetricID': metric_id,
                        'startTime': window_time,
                        'timeWindowData': window_size,
                        'value': metric['acc'],
                        'valueMin': decimal.Decimal(min),
                        'valueMax': decimal.Decimal(max)
                    }
                }
                try:
                    requests.post(url=endpoint_url, data=json.dumps(alert_data_send, cls=DecimalEncoder),
                                  timeout=20)
                except Exception as e:
                    print('ERROR: sent notification endpoint', endpoint_url, e)

                # Insert anomaly in database
                sqlInsert = "INSERT INTO client_anomaly_events (`client_id`,`data_metric_id`, `start_time`," \
                            "`time_window_data`,`value`,`value_max`, `value_min`) VALUES(%s,%s,%s,%s,%s,%s,%s)"
                cursor = sql_connection.cursor()
                cursor.execute(sqlInsert, (client_id, metric_id, window_time, window_size, metric['acc'], max, min))
                sql_connection.commit()







def main(encodedInput):
    # Parse input
    jsonInput = base64.b64decode(encodedInput)
    cleanInput = json.loads(jsonInput)

    # Format timestamp
    cleanInput['windowTime'] = isoparse(cleanInput['windowTime'])

    # Connect to redis and database
    connection = None
    try:
        connection = mysql.connector.connect(
            host="dv-sql-dev.ct8gskz3trds.eu-central-1.rds.amazonaws.com",
            user="dvision",
            passwd="w435tg5g56umh9346tgcb533kf",
            database="dvisiondb"
        )
    except Error as e:
        print(f"The error '{e}' occurred while trying to connect to SQL")
    r = redis.Redis(host='cluster-redis.zmddku.0001.euc1.cache.amazonaws.com', port=6379, decode_responses=True)

    # Obtain id of the data source
    dataSourceID, client_id = getDataSourceID(r, connection, cleanInput['client'],  cleanInput['sourceId'],
                                   cleanInput['sourceType'], cleanInput['sourceSubtype'])
    print(dataSourceID)

    # If we identify the source
    if dataSourceID:
        # Store the metrics
        get_metric_ids_and_create_datapoints(r, cleanInput, dataSourceID, connection, client_id)

    else:  # If we don't
        # return and log the error and client key name
        print("Error: The data source ({}, {}, {}, {}) is not registered in our system!".format(cleanInput['client'],
                                                                                                cleanInput['sourceType'],
                                                                                                cleanInput['sourceSubtype'],
                                                                                                cleanInput['sourceId']))
    connection.close()

def lambda_handler(event, context):
    # parse incoming form data
    print('## EVENTS:', event)
    for e in event['Records']:
        main(e['kinesis']['data'])
    return {
        'statusCode': 200
    }


